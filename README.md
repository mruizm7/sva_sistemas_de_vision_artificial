**SISTEMAS DE VISIÓN ARTIFICIAL**

Este sitio es el repositorio central de los códigos fuentes para el cuatrimestre enero-abril 2022. En el podrá encontrar los código fuente de cada una de las sesiones que se llevarań durante el semestre actual.
Le recomiendo buscar aquí recursos como vídeos, imágenes, archivos texto, etc... que serán útiles para realizar las prácticas y actividades a lo largo del curso.
Este documento se irá actualizando conforme se vaya avanzando durante el cuatrimestre.

---

## Imagen en tonos de grises

En esta carpeta encontrá un código en OCTAVE que le indica lo siguiente:

1. Como cargar una imagen a color en una variable tipo Imagen.
2. Como convertir la imagen a color en tonos de grises.
3. Subdividir la pantalla usando "subplot()" para colocar varias imágenes en pantalla.
4. Usar el comando "title()" para colocar títulos a las imágenes o gráficos que vaya mostrando.

---

## Ecualización de Imagen

En esta carpeta se tienen los código de la semana 2 respecto a ecualización de la imagen.
El **histogramaDeImagen.m** muestra el el total de píxels de cada tono de gris empezando desde el colo negro (0) hasta el color blanco(255).
Muestra una imagen de barras al lado de la imagen **bajos.png** y una barra con los cambios en forma de gradiente de negro a blanco.

El siguiente código **lunes.m** muestra como realizar la ecualización de la imagen y su resultado así como su histograma al lado derecho.
El histograma muestra como se realizó la distribución de la intensidad de los píxels en base a su función de distribución acumulada.

Además cuenta con las imágenes resultantes para que las compare con lo que haga en su equipo.

---

## MEDIAPIPE

Los archivos que se incluyen son una plantilla para llevar a cabo el seguimiento de la yema del dedo índice. 
Para seleccionar un cuadro visible en pantalla.

1. Acceda a la aplicación **MediaPipeCamaraDragged.py**, cambie el número de puerto para su cámara web USb. .
2. Observe hasta que aparezca el cuadro rojo en la pantalla de su equipo.
3. Muestre su **Dedo Indice** a la cámara sobre el rectángulo para moverlo con su dedo.
4. Para dejar de arrastrar el cuadro, es decir soltarlo junte las yemas de sus dedos **Indice y Pulgar**.

---

## HISTOGRAM OF ORIENTED GRADIENT

HOG es una característica utilizada en la visión por computadora para la detección de objetos . 
La técnica calcula histogramas locales de la orientación del gradiente sobre una cuadrícula densa, es decir, sobre áreas distribuidas regularmente en la imagen. 
Tiene puntos en común con SIFT , contextos Shape e histogramas de orientación de contorno, pero se diferencia en particular por el uso de una cuadrícula densa. 
El método es particularmente eficaz para la detección de personas .

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).