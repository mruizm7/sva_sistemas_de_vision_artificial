pkg load image
im = imread('colores.jpg'); %COLOR
img = rgb2gray(im); % TONOS DE GRISES
subplot(1,2,1); % 1 RENGLON A DOS COLUMNAS SOBRE PRIMER CUADRO DE IMAGEN
imshow(im);
title('IMAGEN A COLOR');
subplot(1,2,2); % CAMBIO AL SEGUNDO CUADRO DE IMAGEN
imshow(img);
title('IMAGEN EN TONOS DE GRISES');
