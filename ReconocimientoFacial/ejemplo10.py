import torch
from facenet_pytorch import MTCNN
from facenet_pytorch import InceptionResnetV1 
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt

# Detectar si se dispone de GPU cuda
# ==============================================================================
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
print('Running on device: {}'.format(device))

# Detector MTCNN
# ==============================================================================

encoder = InceptionResnetV1(pretrained='vggface2', classify=False, device=device).eval()

mtcnn = MTCNN(
            select_largest = True,
            min_face_size  = 20,
            thresholds     = [0.6, 0.7, 0.7],
            post_process   = False,
            image_size     = 160,
            device         = device
        )

imagen_1 = Image.open('images/imagen_1.jpg')

# EXTRACCIÓN DE LA CARA EN LA IMAGEN_1 
face = mtcnn.forward(imagen_1)

#GENERACIÓN DEL VECTOR NUMÉRICO EMBEDDING PARA LA CARA
embedding_cara = encoder.forward(face.reshape((1,3,160,160))).detach().cpu()
print(embedding_cara)
# MOSTRAR LA CARA QUE SE EXTRAJO DE LA IMAGEN
fig, ax = plt.subplots(1, 1, figsize=(3, 3))
face = face.permute(1, 2, 0).int().numpy()
ax.imshow(face)
plt.axis('off');

plt.show()



