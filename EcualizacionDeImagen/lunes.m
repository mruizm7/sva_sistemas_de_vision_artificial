close all
clear all
clc
pkg load image
grayscale_image = imread('bajos.png');
figure;
subplot(2,2,1);
imshow(grayscale_image);
title('Imagen en tonos de gris');

subplot(2,2,2);
imhist(grayscale_image);
ylabel('cantidad de pixels');
xlabel('valor de intensidad del pixel');
title('histograma de la imagen');

subplot(2,2,3);
result_image = histeq(double(grayscale_image)/255, 256);
imshow(uint8(255*result_image));
title('Imagen Ecualizada');

subplot(2,2,4);
imhist(uint8(255*result_image),256);
title('Histograma ecualizado');


