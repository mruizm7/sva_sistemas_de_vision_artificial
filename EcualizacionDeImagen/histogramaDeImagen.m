close all
clear all
clc
pkg load image
grayscale_image = imread('bajos.png');
figure;
subplot(1,2,1);
imshow(grayscale_image);
title('Imagen en tonos de gris');

subplot(1,2,2);
imhist(grayscale_image);
ylabel('cantidad de pixels');
xlabel('valor de intensidad del pixel');
title('histograma de la imagen');

